﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3_4_5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> list = new List<IRentable>();
            Video video = new Video("Lord of the Rings");
            Book book = new Book("Percy Jackson");
            list.Add(video);
            list.Add(book);
            RentingConsolePrinter rent = new RentingConsolePrinter();
            rent.DisplayItems(list);
            rent.PrintTotalPrice(list);


            List<IRentable> Lista2 = new List<IRentable>();
            HotItem Science = new HotItem(new Book("Science"));
            Lista2.Add(Science);
            HotItem VideoGames = new HotItem(new Video("World of Warcraft"));
            Lista2.Add(VideoGames);
            RentingConsolePrinter Out2 = new RentingConsolePrinter();
            Out2.DisplayItems(Lista2);
            Out2.PrintTotalPrice(Lista2);

            List<IRentable> flashsale = new List<IRentable>();
            DiscountedItem low1 = new DiscountedItem(video);
            DiscountedItem low2 = new DiscountedItem(book);
            flashsale.Add(low1);
            flashsale.Add(low2);
            RentingConsolePrinter Out3 = new RentingConsolePrinter();
            Out3.DisplayItems(flashsale);
            Out3.PrintTotalPrice(flashsale);


        }
    }
}