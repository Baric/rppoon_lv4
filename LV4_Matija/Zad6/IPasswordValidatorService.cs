﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad6
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
