﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad6
{
    interface IRegistrationValidator
    {
        bool IsUserEntryValid(UserEntry entry);
    }
}
