﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad6
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
