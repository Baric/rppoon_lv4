﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad6
{
    class EmailValidator : IEmailValidatorService
    {
        public bool IsValidAddress(String candidate)
        {
            if (candidate.Contains('@') && (candidate.Contains(".com") || candidate.Contains(".hr")))
            {
                Console.WriteLine("Validan email.");
                return true;
            }
            Console.WriteLine("Nije validan email.");
            return false;
        }
    }
}
