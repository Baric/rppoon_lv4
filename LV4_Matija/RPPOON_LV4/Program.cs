﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV4
{
    class Program
    {
        //Zad1
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> data = dataset.GetData();
            int rows = data.Count;
            int cols = data[0].Count;
            double[][] matrix = new double[rows][];
            for (int i = 0; i < rows; i++)
            {
                matrix[i] = new double[cols];
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    matrix[i][j] = data[i][j];

                }
            }
            return matrix;
        }

        //Zad1
        public double[] PerColumnAverage(double[][] data)
        {
            int row = data.Count();
            int col = data[0].Count();
            double[] results = new double[col];

            for (int i = 0; i < col; i++)
            {

                for (int j = 0; j < row; j++)
                {
                    results[i] += data[j][i];
                }
                results[i] = results[i] / data.Length;
            }

            return results;
        }
        //Zad2

        //Dataset data = new Dataset("D:\\University stuff\\RPPOON\\LV\\LV4\\LV4_Matija\\RPPOON_LV4\\CSVFile");
        //Analyzer3rdParty analyze = new Analyzer3rdParty();
        //Adapter adapter = new Adapter(analyze);
        //Console.WriteLine(adapter.CalculateAveragePerColumn(data));

        //        double[] res = adapter.CalculateAveragePerColumn(data);

        //        foreach (double r in res)
        //        {
        //            Console.WriteLine(r);
        //        }

    }

    //Zad3
    class Book : IRentable
    {
        private readonly double BaseBookPrice = 3.99;
        public String Name { get; private set; }
        public Book(String name) { this.Name = name; }

        public string Description { get { return this.Name; } }
        public double CalculatePrice() { return BaseBookPrice; }

    }

    //Zad4
    //{
    //List<IRentable> list = new List<IRentable>();
    //Video video = new Video("Lord of the Rings");
    //Book book = new Book("Percy Jackson");
    //list.Add(video);
    //list.Add(book);
    //RentingConsolePrinter rent = new RentingConsolePrinter();
    //rent.DisplayItems(list);
    //rent.PrintTotalPrice(list);
    //}

    //Zad5
    class DiscountedItem : RentableDecorator
    {
        private readonly double DiscountedItemBonus = 0.20;

        public DiscountedItem(IRentable rentable) : base(rentable) { }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice() * this.DiscountedItemBonus;
        }
        public override String Description
        {
            get
            {
                return "Now at  " + DiscountedItemBonus * 100 + " % off " + base.Description;
            }
        }
    }

    //Zad6
    class EmailValidator : IEmailValidatorService
    {
        public bool IsValidAddress(String candidate)
        {
            if (candidate.Contains('@') && (candidate.Contains(".com") || candidate.Contains(".hr")))
            {
                Console.WriteLine("Validan email.");
                return true;
            }
            Console.WriteLine("Nije validan email.");
            return false;
        }
    }
}